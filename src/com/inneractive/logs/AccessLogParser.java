/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inneractive.logs;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author david_000
 */
public class AccessLogParser {

    private static final Pattern linePattern = Pattern.compile("HttpRequest\\(GET,(.*),List\\((.*)\\),.*Response-Headers: (.*) sc");
    private static final Pattern urlPattern = Pattern.compile(".*/simpleM2M/(.*)");
    private static final String ENDPOINT = "endpoint";
    private static Set<String> urlParameters = new LinkedHashSet<>();
    private static Set<String> responseHeaders = new LinkedHashSet<>();

    public static void main(String[] args) throws IOException {
        if( args.length != 2){
            System.err.print( "Usage: java -Xmx2048M -jar access_log_parser.jar <input-log-file> <output-log-file>");
            return;
        }
        File inputFile = new File( args[0]);
        File outputFile = new File( args[1]);
        urlParameters.add( ENDPOINT);
        BufferedReader bufferedReader = new BufferedReader( new FileReader( inputFile));
        BufferedWriter bufferedWriter = new BufferedWriter( new FileWriter( outputFile));
        String line;
        System.out.println( "Started reading the keys from " + args[0] + ", writing output to " + args[1]);
        while ((line = bufferedReader.readLine()) != null) {
            line  = checkLine( line);
            if( line == null){
                continue;
            }
            handleLineParameters(line);
        }
//        bufferedWriter.write( StringUtils.join( urlParameters.iterator(), ",") + "," + StringUtils.join( responseHeaders.iterator(), ",") + "\n");
        
        CreateTable table = new CreateTable();
        // clean up all key lists so they will have only the keys we are going to actually use in the "CREATE TABLE" statement        
        // generate and print SQL CREATE TABLE statement for the found fields in the order they were found
        urlParameters = table.removeUnusedKeys( urlParameters);
        responseHeaders = table.removeUnusedKeys( responseHeaders);
        System.out.println( table.generateSqlStatement( StringUtils.join( urlParameters.iterator(), ",") + "," + StringUtils.join( responseHeaders.iterator(), ",")));
        
        bufferedReader = new BufferedReader( new FileReader(inputFile));
        System.out.println( "Started reading the values and writing the output lines");
        String rp;
        while ((line = bufferedReader.readLine()) != null) {
            line  = checkLine( line);
            if( line == null){
                continue;
            }
            Matcher matcher = linePattern.matcher(line);
            if (matcher.find()){
                rp = readLineParameters( matcher.group(1));
                if( rp != null){
                    bufferedWriter.write( rp + "," + readLineResponseHeaders(matcher.group(3)) + "\n");
                } else {
                    System.err.println( "Can not properly parse params, line skipped: " + line);
                }
            } else {
//                System.out.println("NO MATCH in main(): " + line);
            }
        }
        bufferedReader.close();
        bufferedWriter.flush();
        bufferedWriter.close();
        System.out.println( "Parsing completed");        
    }
    
    private static String checkLine( String line){ // check if line contains any unwanted pattern
        if( line.contains("__defineGetter__") || line.contains("mraid.js") || line.contains("favicon.ico") || line.contains("IA_GameTest")){
            return null;
        } else if( line.contains("&amp;")){ // HTML encoded request - looks like the server understands it anyway, so let's handle it as well
            return StringEscapeUtils.unescapeHtml4( line);
        } else{
            return line;
        }
    }

    private static String readLineResponseHeaders(String requestHeaders) throws UnsupportedEncodingException {
//        requestHeaders = StringEscapeUtils.unescapeHtml4( requestHeaders);
        String[] headers = requestHeaders.split("X-IA");
        Map<String, String> lineHeadersMap = new HashMap<>();
        List<String> lineResult = new ArrayList<>();
        CreateTable t = new CreateTable();
        String value;
        for (String header : headers) {
            if (!"".equals(header)) {
                String[] parts = header.split("=");
                lineHeadersMap.put(("X-IA" + parts[0]).toLowerCase(), StringEscapeUtils.unescapeHtml4( java.net.URLDecoder.decode( parts[1], "UTF-8")).trim());
            }
        }
        for (String responseHeader : responseHeaders) {
            lineResult.add( t.convertValue(responseHeader, lineHeadersMap.get(responseHeader)));
        }
        return StringUtils.join(lineResult.iterator(), ",");
    }

    private static String readLineParameters(String url) throws UnsupportedEncodingException {
        Matcher matcher = urlPattern.matcher(url);
        List<String> lineResult = new ArrayList<>();
        if (matcher.find()) {
            String[] endpointAndParameters = matcher.group(1).split( "\\?");
            if( endpointAndParameters == null || endpointAndParameters.length < 2){
//                System.err.println( "Not enough params, skipped line with URL: " + url);
                return null;
            }
            String endpoint = endpointAndParameters[0];
            String[] parameters = endpointAndParameters[1].split("&");
            Map<String, String> lineParametersMap = new HashMap<>();
            CreateTable t = new CreateTable();
            String value;
            String[] parts;
            for (String parameter : parameters) {
                parts = parameter.split( "=");
                value = parts.length > 1 ? parts[1] : "";
                lineParametersMap.put( normalizeValue( parts[0]).toLowerCase(), normalizeValue( value));
            }
            for (String urlParameter : urlParameters) {
                if (ENDPOINT.equals(urlParameter)) {
                    value = endpoint;
                } else {
                    value = lineParametersMap.get(urlParameter.toLowerCase());
                }
                lineResult.add( t.convertValue( urlParameter, value));
            }
        }
        return StringUtils.join( lineResult.iterator(), ",");
    }

    private static void handleLineParameters(String line) throws UnsupportedEncodingException{
        Matcher matcher = linePattern.matcher(line);
        if (matcher.find()) {
            addUrlParameters(matcher.group(1));
            addResponseHeaders(matcher.group(3));
        } else {
//            System.out.println("NO MATCH in handleLineParameters(): " + line);
        }
    }

    private static void addResponseHeaders(String requestHeaders) throws UnsupportedEncodingException{
//        requestHeaders = StringEscapeUtils.unescapeHtml4( requestHeaders);
        String[] headers = requestHeaders.split("X-IA");
        for (String header : headers) {
//            if( header.contains("iA-")){
//                System.err.println( "Header: " + header);
//            }
            if (!"".equals(header)) {
                responseHeaders.add( ("X-IA" + header.split("=")[0]).toLowerCase());
            }
        }
    }

    private static void addUrlParameters(String url) throws UnsupportedEncodingException{
        Matcher matcher = urlPattern.matcher(url);
        if( matcher.find()) {
            String urlParams = matcher.group(1);
//            System.out.println( url);
//            if( urlParams == null || urlParams.length() < 2){
//                System.err.println( "No URL params: " + url);
//                return;
//            }
            String[] splittedParams = urlParams.split("\\?");
            if( splittedParams.length < 2){
//                System.err.println( "No URL params: " + url);
                return;
            }
            
            String[] parameters = splittedParams[1].split("&");
            String p;
            for (String parameter : parameters) {
                p = normalizeValue( parameter.split("=")[0]).toLowerCase();
                if( p.length()== 0){ // looks like the first param is always ""...
                    continue;
                }
                urlParameters.add( p);
            }
        } else {
//            System.out.println("NO MATCH in addUrlParameters(): " + url);
        }
    }
    
    private static String normalizeValue( String value)throws UnsupportedEncodingException {
        return StringUtils.strip( StringEscapeUtils.unescapeHtml4( java.net.URLDecoder.decode( value, "UTF-8")).replaceAll("\\n|\\r", "").trim(), "“");
    }
}