/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inneractive.logs;

//import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author david_000
 */
public class CreateTable {
    private HashMap<String, String> fieldsMap = new HashMap();
    private String name = "AccessLog";
    private final static String DEFAULT_VALUE = " CHAR(50)";
    
    public CreateTable(){
        initRedshift();
    }
    
    public CreateTable( String tableName){
        name = tableName;
        initRedshift();
    }

    private void initMySQL(){        
        fieldsMap.put( "endpoint", "`endpoint` CHAR(50) COMMENT \"Server endpoint for various ad formats, like clientRequestEnhancedHtmlAd\"");
        fieldsMap.put( "aid", "`aid` CHAR(50) COMMENT \"IA application ID\"");
        fieldsMap.put( "dynamicfp", "`dynamicfp` FLOAT(5) COMMENT \"Floor price as specified by the client\"");
        fieldsMap.put( "refurl", "`refurl` CHAR(128) COMMENT \"Both 'refURL' and 'reful' in the log\"");
        fieldsMap.put( "bid", "`bid` CHAR(50)  COMMENT \"Bundle ID: app package name in Android or Appstore app ID for iOs\"");
        fieldsMap.put( "v", "`v` CHAR(10) COMMENT \"IA m2m protocol server version\"");
        fieldsMap.put( "ua", "`ua` CHAR(255) COMMENT \"Client user agent\"");
        fieldsMap.put( "cip", "`cip` CHAR(20) COMMENT \"Client IP\"");
        fieldsMap.put( "f", "`f` CHAR(10) COMMENT \"IA ad properties binary mask\"");
        fieldsMap.put( "nt", "`nt` CHAR(10) COMMENT \"Network type, e.g. 'WiFi', '3G'\"");
        fieldsMap.put( "l", "`l` CHAR(4) COMMENT \"Location / Country code, e.g. GB\"");
        fieldsMap.put( "ciso", "`ciso` CHAR(3) COMMENT \"Country code\"");
        fieldsMap.put( "lg", "`lg` CHAR(50) COMMENT \"Coordinates like -37.813835,145.001755\"");
        fieldsMap.put( "aaid", "`aaid` CHAR(50) COMMENT \"Both 'aaID' and 'aaid' in the log. Client Android advertising ID in plain text\"");
        fieldsMap.put( "g", "`g` CHAR(2) COMMENT \"Gender: m, f\"");
        fieldsMap.put( "fs", "`fs` CHAR(5) COMMENT \"?? VALUES found: true, false\"");
        fieldsMap.put( "a", "`a` INT(3) COMMENT \"Age, e.g. 17\"");
        fieldsMap.put( "asha", "`asha` CHAR(50) COMMENT \"SHA-1 of client Android advertising ID\"");
        fieldsMap.put( "amd", "`amd` CHAR(50) COMMENT \"MD5 of client Android advertising ID\"");
        fieldsMap.put( "iesha", "`iesha` CHAR(50) COMMENT \"?? SHA-1 of client IMEI\"");
        fieldsMap.put( "iemd", "`iemd` CHAR(50) COMMENT \"?? MD5 of client IMEI\"");
        fieldsMap.put( "dnt", "`dnt` CHAR(5) COMMENT \"Do not track value: false or true\"");
        fieldsMap.put( "appv", "`appv` CHAR(20) COMMENT \"Client app version\"");
        fieldsMap.put( "dml", "`dml` CHAR(35) COMMENT \"Client device model\"");
        fieldsMap.put( "mcc", "`mcc` INT(5) COMMENT \"Mobile carrier code\"");
        fieldsMap.put( "mnc", "`mnc` INT( 5) COMMENT \"Mobile network code\"");
        fieldsMap.put( "t", "`t` TIMESTAMP COMMENT \"?? Looks like a UNIX timestamp\"");
        fieldsMap.put( "crn", "`crn` CHAR(5) COMMENT \"MNO name in plain text (carrier name), e.g. 'Sprint'\"");
        fieldsMap.put( "w", "`w` INT(5) COMMENT \"Device screen width in px\"");
        fieldsMap.put( "h", "`h` INT( 5) COMMENT \"Device screen height in px\"");
        fieldsMap.put( "o", "`o` CHAR(1) COMMENT \"Device orientation: 'l' for landscape, 'p' for portrait\"");
        fieldsMap.put( "fromsdk", "`fromsdk` CHAR(5) COMMENT \"Boolean, 'true' if sent via SDK, 'false' otherwise\"");
        fieldsMap.put( "idfa", "`idfa` CHAR(50) COMMENT \"Client iOS device ID in plain text\"");
        fieldsMap.put( "med", "`med` CHAR(30) COMMENT \"Mediator' name if the request is under the mediation, e.g. 'mopub'\"");
        fieldsMap.put( "isdks", "`isdks` CHAR(30) COMMENT \"?? Installed SDKs? Found values: MM, CFB (millenial medai, FB)?\"");
//        fieldsMap.put( "po", "`po` INT(4) COMMENT \"?? NewRelic param? Found values:3200, 3201, 659, 551\"");
        fieldsMap.put( "ow", "`ow` INT(5) COMMENT \"?? Looks the same like 'rw'\"");
        fieldsMap.put( "oh", "`oh` INT(5) COMMENT \"?? Looks the same like 'rh'\"");        
        fieldsMap.put( "ismd", "`ismd` CHAR(50) COMMENT \"?? some device ID in MD5?\"");        
        fieldsMap.put( "ia-host", "`ia-host` CHAR(50) COMMENT \"?? Client host name?\"");        
        fieldsMap.put( "cid", "`cid` CHAR(50) COMMENT \"Client ID as defined by the Server. Deprecated\"");        
        fieldsMap.put( "orientation", "`orientation` CHAR(50) COMMENT \"Device orientation. The same like 'o', Values: portrait, landscape\"");
        fieldsMap.put( "appname", "`appname` CHAR(50) COMMENT \"App name as know to the publisher, e.g. 'accuweather'\"");        
        fieldsMap.put( "rw", "`rw` INT(4) COMMENT \"Requested ad width in px\"");        
        fieldsMap.put( "rh", "`rh` INT(4) COMMENT \"Requested ad height in px\"");        
        fieldsMap.put( "k", "`k` CHAR(100) COMMENT \"Keywords\"");        
        fieldsMap.put( "iabcategories", "`iabcategories` CHAR(50) COMMENT \"List of IAB categories, e.g. IAB1-10\"");
        fieldsMap.put( "mw", "`mw` CHAR(5) COMMENT \"Mobile Wbe indication: true/false\"");        
        fieldsMap.put( "mmd", "`mmd` CHAR(50) COMMENT \"?? Yet another client ID...\"");        
        fieldsMap.put( "msha", "`msha` CHAR(50) COMMENT \"?? Yet another client ID...\"");        
        fieldsMap.put( "issha", "`issha` CHAR(50) COMMENT \"?? Yet another client ID...\"");        
        fieldsMap.put( "idfv", "`idfv` CHAR(50) COMMENT \"?? Vendor' ID\"");
        fieldsMap.put( "c", "`c` CHAR(50) COMMENT \"?? Kind of app ID / name? c=ABC%20News%20Smartphone%20Site%7Centertainment\"");        
        fieldsMap.put( "dmd", "`dmd` CHAR(50) COMMENT \"?? Yet another device ID, probably in MD5\"");        
        fieldsMap.put( "dsha", "`dsha` CHAR(50) COMMENT \"?? Yet another device ID, probably in SHA-1\"");        
        fieldsMap.put( "ia-x-nokia-bearer", "`ia-x-nokia-bearer` CHAR(50) COMMENT \"E.g. iA-X-Nokia-BEARER=GPRS\"");        
        fieldsMap.put( "eid", "`eid`CHAR(50) COMMENT \"?? Yet another device ID?\"");
        fieldsMap.put( "mac", "`mac`CHAR(50) COMMENT \"?? MAC address in plain text?\"");        
        fieldsMap.put( "damd", "`damd` CHAR(50) COMMENT \"Nokia?? Device ID in MD5 for WP, e.g. 'damd=a4b99fa781f0600afa251a5ba171521d'\"");        
        fieldsMap.put( "dasha", "`dasha` CHAR(50) COMMENT \"Nokia?? Device ID in SHA-1 for WP, e.g. 'dasha=57862c99de46941a806ff9abaa65642da74fc323'\"");        
        fieldsMap.put( "fmt", "`fmt` CHAR(10) COMMENT \"?? Requested format for the response? 'fmt=xml'\"");        
        fieldsMap.put( "ia-x-client-ip", "`ia-x-client-ip` CHAR(20) COMMENT \"Client IP reported by the Publisher\"");
        fieldsMap.put( "rd", "`rd` CHAR(50) COMMENT \"Some ID, e.g. rd=e4f1aac0256568dd3d63831b5f099691\"");
        fieldsMap.put( "sh", "`sh` INT(4) COMMENT \"No diff with device width and height, even if designed differently. sh=400&sw=240&h=400&w=240\"");
        fieldsMap.put( "sw", "`sw` INT(4) COMMENT \"No diff with device width and height, even if designed differently. sh=400&sw=240&h=400&w=240\"");
        fieldsMap.put( "browser", "`browser` CHAR(20) COMMENT \"Browser identification. Values: app_j2me\"");
        fieldsMap.put( "idd", "`idd` INT(5) COMMENT \"Some ID. Values: 7244,4249,9403...\"");
        fieldsMap.put( "devip", "`devip` CHAR(30) COMMENT \"Device IP\"");
        fieldsMap.put( "ia-x-att-deviceid", "`ia-x-att-deviceid` CHAR(50) COMMENT \"ATT device ID like &iA-x-att-deviceid=Huawei-U8665/HuaweiU8665B037\"");
        fieldsMap.put( "ia-x-wap-client-ip", "`ia-x-wap-client-ip` CHAR(50) COMMENT \"Client IP, e.g. &iA-X-WAP-Client-IP=10.48.150.168\"");
        fieldsMap.put( "plat", "`plat` CHAR(50) COMMENT \"Deprecated. Platform. Values found: j2me\"");
        fieldsMap.put( "isg2", "`isg2` CHAR(20) COMMENT \"Some ID, e.g. &isg2=AUS9-BDgRDxEPBEI2U1VvUQ8BDZEPEQ8\"");        
//        fieldsMap.put( "waid", "`waid` CHAR(50) COMMENT \"Some ID, e.g. &waid=75bed051478ae761f074f2a0d5c85c3c\"");
        fieldsMap.put( "ia-msisdn", "`ia-msisdn` CHAR(50) COMMENT \"Nokia device MSISDN e.g. &iA-MSISDN=962772210867\"");        
//        fieldsMap.put( "ia-x-orange-roaming", "`ia-x-orange-roaming` CHAR(50) COMMENT \"E.g. iA-X-Orange-Roaming=NO\"");
//        fieldsMap.put( "ia-x-ee-mig", "`ia-x-ee-mig` CHAR(50) COMMENT \"E.g. iA-X-EE-MIG=1\"");        
        fieldsMap.put( "ia-x-ee-client-ip", "`ia-x-ee-client-ip` CHAR(30) COMMENT \" Client IP, e.g. &iA-X-EE-Client-IP=10.178.108.19\"");
        fieldsMap.put( "ia-x-operator-domain", "`ia-x-operator-domain` CHAR(50) COMMENT \"E.g. iA-X-Operator-Domain=orange.co.uk\"");        
        fieldsMap.put( "ia-x-nokia-ipaddress", "`ia-x-nokia-ipaddress` CHAR(30) COMMENT \"Client IP, e.g. iA-X-Nokia-ipaddress=10.178.108.19\"");
        fieldsMap.put( "ia-client-ip", "`ia-client-ip` CHAR(50) COMMENT \"Client IP, e.g. &iA-Client-IP=10.180.112.226\"");        
        fieldsMap.put( "ia-x-up-forwarded-for", "`ia-x-up-forwarded-for` CHAR(20) COMMENT \"Client IP reported by the Publisher. See also 'ia-x-client-ip'\"");
//        fieldsMap.put( "ia-cuda_cliip", "`ia-cuda_cliip` CHAR(20) COMMENT \"?? iA-CUDA_CLIIP=10.86.5.59\"");        
        fieldsMap.put( "x-ia-pricing", "`x-ia-pricing` CHAR(5) COMMENT \"Response pricing model: CPC, CPM\"");        
        fieldsMap.put( "x-ia-pricing-value", "`x-ia-pricing-value` FLOAT COMMENT \"Response price. Currently divided into 1,000 for CPM responses\"");
        fieldsMap.put( "x-ia-pricing-currency", "`x-ia-pricing-currency` CHAR(3) COMMENT \"Always USD at the moment\"");        
        fieldsMap.put( "x-ia-ad-width", "`x-ia-ad-width` INT(4) COMMENT \"Returned ad width in px\"");        
        fieldsMap.put( "x-ia-ad-height", "`x-ia-ad-height` INT(4) COMMENT \"Returned ad height in px\"");
        fieldsMap.put( "x-ia-error", "`x-ia-error` CHAR(50) COMMENT \"Error: OK | House Ad | Internal error | ... others\"");        
        fieldsMap.put( "x-ia-adnetwork", "`x-ia-adnetwork` CHAR(50) COMMENT \"The demand partner who responded. For the errors/house ads it is always 'Inneractive 360'\"");        
        fieldsMap.put( "x-ia-ad-type", "`x-ia-ad-type` INT(3) COMMENT \"?? Returned ad type\"");
    }
    
    private void initRedshift(){        
        fieldsMap.put( "endpoint", "endpoint CHAR(50)");
        fieldsMap.put( "aid", "aid CHAR(255)");
        fieldsMap.put( "dynamicfp", "dynamicfp FLOAT");
        fieldsMap.put( "refurl", "refurl VARCHAR(1024)");
        fieldsMap.put( "bid", "bid CHAR(255)");
        fieldsMap.put( "v", "v CHAR(50)");
        fieldsMap.put( "ua", "ua VARCHAR(1024)");
        fieldsMap.put( "cip", "cip CHAR(50)");
        fieldsMap.put( "f", "f SMALLINT");
        fieldsMap.put( "nt", "nt CHAR(10)");
        fieldsMap.put( "l", "l VARCHAR(50)");
        fieldsMap.put( "ciso", "ciso CHAR(3)");
        fieldsMap.put( "lg", "lg VARCHAR(64)");
        fieldsMap.put( "aaid", "aaid CHAR(50)");
        fieldsMap.put( "g", "g CHAR(50)");
        fieldsMap.put( "fs", "fs CHAR(5)");
        fieldsMap.put( "a", "a SMALLINT");
        fieldsMap.put( "asha", "asha CHAR(50)");
        fieldsMap.put( "amd", "amd CHAR(50)");
        fieldsMap.put( "iesha", "iesha CHAR(50)");
        fieldsMap.put( "iemd", "iemd CHAR(50)");
        fieldsMap.put( "dnt", "dnt CHAR(5)");
        fieldsMap.put( "appv", "appv CHAR(50)");
        fieldsMap.put( "dml", "dml VARCHAR(64)");
        fieldsMap.put( "mcc", "mcc INT");
        fieldsMap.put( "mnc", "mnc INT");
//        fieldsMap.put( "t", "t CHAR(100)");
        fieldsMap.put( "crn", "crn VARCHAR(64)");
        fieldsMap.put( "w", "w SMALLINT");
        fieldsMap.put( "h", "h SMALLINT");
        fieldsMap.put( "o", "o CHAR(1)");
        fieldsMap.put( "fromsdk", "fromsdk CHAR(5)");
        fieldsMap.put( "idfa", "idfa CHAR(50)");
        fieldsMap.put( "med", "med CHAR(30)");
        fieldsMap.put( "isdks", "isdks CHAR(30)");
//        fieldsMap.put( "po", "`po` INT(4) COMMENT \"?? NewRelic param? Found values:3200, 3201, 659, 551\"");
        fieldsMap.put( "ow", "ow SMALLINT");
        fieldsMap.put( "oh", "oh SMALLINT");        
        fieldsMap.put( "ismd", "ismd CHAR(50)");        
        fieldsMap.put( "ia-host", "iahost CHAR(50)");        
        fieldsMap.put( "cid", "cid CHAR(50)");        
        fieldsMap.put( "orientation", "orientation CHAR(50)");
        fieldsMap.put( "appname", "appname CHAR(50)");        
        fieldsMap.put( "rw", "rw SMALLINT");        
        fieldsMap.put( "rh", "rh SMALLINT");        
        fieldsMap.put( "k", "k VARCHAR(1024)");        
        fieldsMap.put( "iabcategories", "iabcategories VARCHAR(1024)");
        fieldsMap.put( "mw", "mw CHAR(5)");        
        fieldsMap.put( "mmd", "mmd CHAR(50)");        
        fieldsMap.put( "msha", "msha CHAR(50)");        
        fieldsMap.put( "issha", "issha CHAR(50)");        
        fieldsMap.put( "idfv", "idfv CHAR(50)");
        fieldsMap.put( "c", "c CHAR(50)");        
        fieldsMap.put( "dmd", "dmd CHAR(50)");        
        fieldsMap.put( "dsha", "dsha CHAR(50)");        
        fieldsMap.put( "ia-x-nokia-bearer", "iaxnokiabearer CHAR(50)");        
        fieldsMap.put( "eid", "eid CHAR(50)");
        fieldsMap.put( "mac", "mac CHAR(50)");        
        fieldsMap.put( "damd", "damd CHAR(50)");        
        fieldsMap.put( "dasha", "dasha CHAR(50)");        
        fieldsMap.put( "fmt", "fmt CHAR(10)");        
        fieldsMap.put( "ia-x-client-ip", "iaxclientip CHAR(20)");
        fieldsMap.put( "rd", "rd CHAR(50)");
        fieldsMap.put( "sh", "sh SMALLINT");
        fieldsMap.put( "sw", "sw SMALLINT");
        fieldsMap.put( "browser", "browser CHAR(20)");
        fieldsMap.put( "idd", "idd SMALLINT");
        fieldsMap.put( "devip", "devip CHAR(30)");
        fieldsMap.put( "ia-x-att-deviceid", "iaxattdeviceid CHAR(50)");
        fieldsMap.put( "ia-x-wap-client-ip", "iaxwapclientip CHAR(50)");
        fieldsMap.put( "plat", "plat CHAR(10)");
        fieldsMap.put( "isg2", "isg2 CHAR(20)");        
//        fieldsMap.put( "waid", "`waid` CHAR(50) COMMENT \"Some ID, e.g. &waid=75bed051478ae761f074f2a0d5c85c3c\"");
        fieldsMap.put( "ia-msisdn", "iamsisdn CHAR(50)");        
//        fieldsMap.put( "ia-x-orange-roaming", "`ia-x-orange-roaming` CHAR(50) COMMENT \"E.g. iA-X-Orange-Roaming=NO\"");
//        fieldsMap.put( "ia-x-ee-mig", "`ia-x-ee-mig` CHAR(50) COMMENT \"E.g. iA-X-EE-MIG=1\"");        
        fieldsMap.put( "ia-x-ee-client-ip", "iaxeeclientip CHAR(30)");
        fieldsMap.put( "ia-x-operator-domain", "iaxoperatordomain CHAR(50)");        
        fieldsMap.put( "ia-x-nokia-ipaddress", "iaxnokiaipaddress CHAR(30)");
        fieldsMap.put( "ia-client-ip", "iaclientip CHAR(20)");        
        fieldsMap.put( "ia-x-up-forwarded-for", "iaxupforwardedfor CHAR(20)");
//        fieldsMap.put( "ia-cuda_cliip", "`ia-cuda_cliip` CHAR(20) COMMENT \"?? iA-CUDA_CLIIP=10.86.5.59\"");        
        fieldsMap.put( "x-ia-pricing", "xiapricing CHAR(5)");        
        fieldsMap.put( "x-ia-pricing-value", "xiapricingvalue FLOAT");
//        fieldsMap.put( "x-ia-pricing-currency", "xiapricingcurrency CHAR(3)");        
        fieldsMap.put( "x-ia-ad-width", "xiaadwidth SMALLINT");        
        fieldsMap.put( "x-ia-ad-height", "xiaadheight SMALLINT");
        fieldsMap.put( "x-ia-error", "xiaerror CHAR(50)");        
        fieldsMap.put( "x-ia-adnetwork", "xiaadnetwork CHAR(50)");        
        fieldsMap.put( "x-ia-ad-type", "xiaadtype SMALLINT");
    }
    
    String convertValue( String key, String value){
        String v = fieldsMap.get( key);
        if( value != null && value.length() > 0){
            try{
                if( v.contains( "INT")){
                    Integer.parseInt( value);
                } else if( v.contains( "FLOAT")){
                    Double.parseDouble( value);                    
                }
            } catch( NumberFormatException e){
                return "";
            }
            return "\"" + value.replace("\"", "\"\"") + "\"";
        } else
            return "";
    }
        
    public String generateSqlStatement( String commaSeparatedKeys){
        Set<String> fields = new LinkedHashSet();
        fields.addAll( java.util.Arrays.asList( StringUtils.split( commaSeparatedKeys, ",")));
        return generateSqlStatement( fields);
    }
    
    public String generateSqlStatement( Set<String> keys){
        // fill the values array:
        // if we got a pre-defined value for this key from the HashMap - use it, otherwise create a new filed for this key with the default value
        // keep the original order of fields
        Set<String> sqlFields = new LinkedHashSet<>();
        for( String key: keys){
            String value = fieldsMap.get(key);
            sqlFields.add( value == null ? ("`" + key+ "`" + DEFAULT_VALUE) : value);
        }
//        return "CREATE TABLE " + name + "(\n"+ StringUtils.join( sqlFields.iterator(), ",\n") + "\n) ENGINE=BRIGHTHOUSE;";
        return "CREATE TABLE " + name + "(\n"+ StringUtils.join( sqlFields.iterator(), ",\n") + "\n);";
    }
    
    public Set<String> removeUnusedKeys( Set<String> keys){
        Set<String> sqlFields = new LinkedHashSet<>();
        for( String key: keys){
            if( fieldsMap.get(key) != null){
                sqlFields.add( key);
            }
        }
        return sqlFields;
    }
    
    
    public static void main(String[] args) {    
        System.out.println( (new CreateTable()).generateSqlStatement( "endpoint,aid,dynamicfp,refurl,bid,v,ua,cip,f,nt,l,lg,aaid,g,fs,a,asha,amd,iesha,iemd,dnt,appv,dml,ciso,mcc,mnc,t,crn,w,h,o,fromsdk,idfa,med,isdks,po,ow,oh,ismd,ia-host,cid,orientation,appname,rw,rh,hid,k,osudi,test,time,iabcategories,cuid,mw,timestamp,mmd,msha,issha,idfv,c,dmd,dsha,ia-via,ia-accept,ia-accept-charset,ia-cookie,hacc,vacc,tacc,noadstring,callback,ia-accept-encoding,mn,ia-x-nokia-gateway-id,ia-x-nokia-bearer,ia-x-nokia-maxdownlinkbitrate,ia-x-nokia-maxuplinkbitrate,eid,at,ia-x-up-calling-line-id,ia-x-source-id,ia-x-up-bear-type,ia-x-wap-profile,ia-x-forward-for,ia-x-uidh,ia-x-bluecoat-via,ia-x-forwarded-for,mac,damd,dasha,fmt,ia-x-proxy-id,ia-x-gateway,ia-o2gw-id,random,verbose,cacherandom,guid,ia-x-client-ip,rd,sh,sw,browser,idd,ia-x-loop-control,devip,ia-x-att-deviceid,ia-referer,ia-x-unity-version,ia-x-wap-client-ip,plat,response,adcount,format,ia-wap-connection,ia-te,ia-x-wap-wtlsencryptiontype,ia-x-wap-bearerinfo,ia-x-up-subno,ia-tm_user-id,isg2,osv,waid,ia-msisdn,ia-x-imforwards,zip,ia-x-nokia-remotesocket,ia-x-nokia-localsocket,ia-x-network-info,ia-proxy-connection,ia-x-orange-roaming,ia-x-ee-mig,ia-x-ee-brand-id,ia-x-ee-client-ip,ia-x-operator-domain,ia-x-nokia-ipaddress,ia-x-orange-rat,ia-content-type,ia-connection,ia-cache-control,ia-content-length,ia-servicecontrolinfo,ia-x-nokia-gid,ia-x-vfprovider,ia-x-vfstatus,ia-x-nokia-connection_mode,ia-transfer-encoding,ia-client-ip,ia-x-turbopage,ia-x-content-opt,ia-x--------------,ia-x-up-forwarded-for,ia-cuda_cliip,ia-accept-language,needcache,ia-x-nokia-musicshop-version,ia-x-nokia-musicshop-bearer,ia-x-mdg-user,ia-x-wapipaddr,visitdsttime,ia-x-amobee-1,ia-wapconnection,uip,ia-x-vf-acr,ia-x-via,x-ia-pricing,x-ia-pricing-value,x-ia-pricing-currency,x-ia-ad-width,x-ia-ad-height,x-ia-cid,x-ia-session,x-ia-error,x-ia-adnetwork,x-ia-ad-type"));
    }
    
}

/* -- script for Infobright data loading --

use inneractive;
DROP TABLE AccessLog;
CREATE TABLE AccessLog(
`endpoint` CHAR(50) COMMENT "Server endpoint for various ad formats, like clientRequestEnhancedHtmlAd",
`aid` VARCHAR(1024) COMMENT "IA application ID",
`dynamicfp` FLOAT(5) COMMENT "Floor price as specified by the client",
`refurl` VARCHAR(4096) COMMENT "Both 'refURL' and 'reful' in the log",
`bid` CHAR(255)  COMMENT "Bundle ID: app package name in Android or Appstore app ID for iOs",
`v` VARCHAR(1024) COMMENT "IA m2m protocol server version",
`ua` VARCHAR(1024) COMMENT "Client user agent",
`cip` CHAR(255) COMMENT "Client IP",
`f` CHAR(50) COMMENT "IA ad properties binary mask",
`nt` CHAR(10) COMMENT "Network type, e.g. 'WiFi', '3G'",
`l` CHAR(255) COMMENT "Location / Country code, e.g. GB",
`lg` CHAR(50) COMMENT "Coordinates like -37.813835,145.001755",
`aaid` CHAR(50) COMMENT "Both 'aaID' and 'aaid' in the log. Client Android advertising ID in plain text",
`g` CHAR(50) COMMENT "Gender: m, f",
`fs` CHAR(5) COMMENT "?? VALUES found: true, false",
`a` INT(3) COMMENT "Age, e.g. 17",
`asha` CHAR(50) COMMENT "SHA-1 of client Android advertising ID",
`amd` CHAR(50) COMMENT "MD5 of client Android advertising ID",
`iesha` CHAR(50) COMMENT "?? SHA-1 of client IMEI",
`iemd` CHAR(50) COMMENT "?? MD5 of client IMEI",
`dnt` CHAR(5) COMMENT "Do not track value: false or true",
`appv` CHAR(20) COMMENT "Client app version",
`dml` CHAR(255) COMMENT "Client device model",
`ciso` CHAR(3) COMMENT "Country code",
`mcc` INT(5) COMMENT "Mobile carrier code",
`mnc` INT( 5) COMMENT "Mobile network code",
`t` TIMESTAMP COMMENT "?? Looks like a UNIX timestamp",
`crn` CHAR(255) COMMENT "MNO name in plain text (carrier name), e.g. 'Sprint'",
`w` INT(5) COMMENT "Device screen width in px",
`h` INT( 5) COMMENT "Device screen height in px",
`o` CHAR(10) COMMENT "Device orientation: 'l' for landscape, 'p' for portrait",
`fromsdk` CHAR(5) COMMENT "Boolean, 'true' if sent via SDK, 'false' otherwise",
`idfa` CHAR(50) COMMENT "Client iOS device ID in plain text",
`med` CHAR(30) COMMENT "Mediator' name if the request is under the mediation, e.g. 'mopub'",
`isdks` CHAR(30) COMMENT "?? Installed SDKs? Found values: MM, CFB (millenial medai, FB)?",
`po` INT(4) COMMENT "?? NewRelic param? Found values:3200, 3201, 659, 551",
`ow` INT(5) COMMENT "?? Looks the same like 'rw'",
`oh` INT(5) COMMENT "?? Looks the same like 'rh'",
`ismd` CHAR(50) COMMENT "?? some device ID in MD5?",
`ia-host` CHAR(50) COMMENT "?? Client host name?",
`cid` CHAR(50) COMMENT "Client ID as defined by the Server. Deprecated",
`orientation` CHAR(50) COMMENT "Device orientation. The same like 'o', Values: portrait, landscape",
`appname` CHAR(50) COMMENT "App name as know to the publisher, e.g. 'accuweather'",
`rw` INT(4) COMMENT "Requested ad width in px",
`rh` INT(4) COMMENT "Requested ad height in px",
`hid` CHAR(50) COMMENT "Currently not used",
`k` VARCHAR(1024) COMMENT "Keywords",
`osudi` CHAR(50) COMMENT "Currently not used",
`test` CHAR(50) COMMENT "Currently not used",
`time` CHAR(50) COMMENT "Currently not used",
`iabcategories` CHAR(255) COMMENT "List of IAB categories, e.g. IAB1-10",
`cuid` CHAR(50) COMMENT "Currently not used",
`mw` CHAR(5) COMMENT "Mobile Wbe indication: true/false",
`timestamp` CHAR(50) COMMENT "Currently not used",
`mmd` CHAR(50) COMMENT "?? Yet another client ID...",
`msha` CHAR(50) COMMENT "?? Yet another client ID...",
`issha` CHAR(50) COMMENT "?? Yet another client ID...",
`idfv` CHAR(50) COMMENT "?? Vendor' ID",
`c` CHAR(50) COMMENT "?? Kind of app ID / name? c=ABC%20News%20Smartphone%20Site%7Centertainment",
`dmd` CHAR(255) COMMENT "?? Yet another device ID, probably in MD5",
`dsha` CHAR(50) COMMENT "?? Yet another device ID, probably in SHA-1",
`ia-via` CHAR(255) COMMENT "Currently not used",
`ia-accept` CHAR(50) COMMENT "Currently not used",
`ia-accept-charset` CHAR(50) COMMENT "Currently not used",
`ia-cookie` CHAR(255) COMMENT "Currently not used",
`hacc` CHAR(50) COMMENT "Currently not used",
`tacc` CHAR(50) COMMENT "Currently not used",
`noadstring` CHAR(50) COMMENT "Currently not used",
`callback` CHAR(255) COMMENT "Currently not used",
`ia-accept-encoding` CHAR(50) COMMENT "Currently not used",
`mn` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-gateway-id` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-bearer` CHAR(50) COMMENT "E.g. iA-X-Nokia-BEARER=GPRS",
`ia-x-nokia-maxdownlinkbitrate` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-maxuplinkbitrate` VARCHAR(1024) COMMENT "Currently not used",
`eid`CHAR(50) COMMENT "?? Yet another device ID?",
`at` CHAR(50) COMMENT "Currently not used",
`ia-x-up-calling-line-id` VARCHAR(2000) COMMENT "Currently not used",
`ia-x-source-id` CHAR(50) COMMENT "Currently not used",
`ia-x-up-bear-type` CHAR(50) COMMENT "Currently not used",
`ia-x-wap-profile` CHAR(50) COMMENT "Currently not used",
`ia-x-forward-for` CHAR(255) COMMENT "Currently not used",
`ia-x-uidh` CHAR(50) COMMENT "Currently not used",
`ia-x-bluecoat-via` CHAR(255) COMMENT "Currently not used",
`ia-x-forwarded-for` CHAR(50) COMMENT "Currently not used",
`mac`CHAR(50) COMMENT "?? MAC address in plain text?",
`damd` CHAR(50) COMMENT "Nokia?? Device ID in MD5 for WP, e.g. 'damd=a4b99fa781f0600afa251a5ba171521d'",
`dasha` CHAR(50) COMMENT "Nokia?? Device ID in SHA-1 for WP, e.g. 'dasha=57862c99de46941a806ff9abaa65642da74fc323'",
`fmt` CHAR(50) COMMENT "?? Requested format for the response? 'fmt=xml'",
`ia-x-proxy-id` CHAR(50) COMMENT "Currently not used",
`ia-x-gateway` CHAR(50) COMMENT "Currently not used",
`ia-o2gw-id` CHAR(50) COMMENT "Currently not used",
`random` CHAR(50) COMMENT "Currently not used",
`verbose` CHAR(50) COMMENT "Currently not used",
`cacherandom` CHAR(50) COMMENT "Currently not used",
`guid` CHAR(50) COMMENT "Currently not used",
`ia-x-client-ip` CHAR(255) COMMENT "Client IP reported by the Publisher",
`rd` CHAR(50) COMMENT "Some ID, e.g. rd=e4f1aac0256568dd3d63831b5f099691",
`sh` INT(4) COMMENT "No diff with device width and height, even if designed differently. sh=400&sw=240&h=400&w=240",
`sw` INT(4) COMMENT "No diff with device width and height, even if designed differently. sh=400&sw=240&h=400&w=240",
`browser` CHAR(20) COMMENT "Browser identification. Values: app_j2me",
`idd` INT(5) COMMENT "Some ID. Values: 7244,4249,9403...",
`ia-x-loop-control` CHAR(50) COMMENT "Currently not used",
`devip` CHAR(255) COMMENT "Device IP",
`ia-x-att-deviceid` CHAR(50) COMMENT "ATT device ID like &iA-x-att-deviceid=Huawei-U8665/HuaweiU8665B037",
`ia-referer` CHAR(50) COMMENT "Currently not used",
`ia-x-unity-version` CHAR(255) COMMENT "Currently not used",
`ia-x-wap-client-ip` CHAR(50) COMMENT "Client IP, e.g. &iA-X-WAP-Client-IP=10.48.150.168",
`plat` CHAR(50) COMMENT "Deprecated. Platform. Values found: j2me",
`response` CHAR(50) COMMENT "Currently not used",
`adcount` CHAR(50) COMMENT "Currently not used",
`format` CHAR(50) COMMENT "Currently not used",
`ia-wap-connection` CHAR(50) COMMENT "Currently not used",
`ia-te` CHAR(50) COMMENT "Currently not used",
`ia-x-wap-wtlsencryptiontype` CHAR(50) COMMENT "Currently not used",
`ia-x-wap-bearerinfo` CHAR(50) COMMENT "Currently not used",
`ia-x-up-subno` CHAR(50) COMMENT "Currently not used",
`ia-tm_user-id` CHAR(50) COMMENT "Currently not used",
`isg2` CHAR(20) COMMENT "Some ID, e.g. &isg2=AUS9-BDgRDxEPBEI2U1VvUQ8BDZEPEQ8",
`osv` CHAR(50) COMMENT "Currently not used",
`waid` CHAR(50) COMMENT "Some ID, e.g. &waid=75bed051478ae761f074f2a0d5c85c3c",
`ia-msisdn` CHAR(50) COMMENT "Nokia device MSISDN e.g. &iA-MSISDN=962772210867",
`ia-x-imforwards` CHAR(50) COMMENT "Currently not used",
`zip` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-remotesocket` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-localsocket` CHAR(50) COMMENT "Currently not used",
`ia-x-network-info` CHAR(50) COMMENT "Currently not used",
`ia-proxy-connection` CHAR(50) COMMENT "Currently not used",
`ia-x-orange-roaming` CHAR(50) COMMENT "E.g. iA-X-Orange-Roaming=NO",
`ia-x-ee-mig` CHAR(50) COMMENT "E.g. iA-X-EE-MIG=1",
`ia-x-ee-brand-id` CHAR(50) COMMENT "Currently not used",
`ia-x-ee-client-ip` CHAR(30) COMMENT " Client IP, e.g. &iA-X-EE-Client-IP=10.178.108.19",
`ia-x-operator-domain` CHAR(50) COMMENT "E.g. iA-X-Operator-Domain=orange.co.uk",
`ia-x-nokia-ipaddress` CHAR(30) COMMENT "Client IP, e.g. iA-X-Nokia-ipaddress=10.178.108.19",
`ia-x-orange-rat` CHAR(50) COMMENT "Currently not used",
`ia-content-type` CHAR(50) COMMENT "Currently not used",
`ia-connection` CHAR(50) COMMENT "Currently not used",
`ia-cache-control` CHAR(50) COMMENT "Currently not used",
`ia-content-length` CHAR(50) COMMENT "Currently not used",
`ia-servicecontrolinfo` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-gid` CHAR(50) COMMENT "Currently not used",
`ia-x-vfprovider` CHAR(50) COMMENT "Currently not used",
`ia-x-vfstatus` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-connection_mode` CHAR(50) COMMENT "Currently not used",
`ia-transfer-encoding` CHAR(50) COMMENT "Currently not used",
`ia-client-ip` CHAR(50) COMMENT "Client IP, e.g. &iA-Client-IP=10.180.112.226",
`ia-x-turbopage` CHAR(50) COMMENT "Currently not used",
`ia-x-content-opt` CHAR(50) COMMENT "Currently not used",
`ia-x--------------` CHAR(50) COMMENT "Currently not used",
`ia-x-up-forwarded-for` CHAR(50) COMMENT "Client IP reported by the Publisher. See also 'ia-x-client-ip'",
`ia-cuda_cliip` CHAR(20) COMMENT "?? iA-CUDA_CLIIP=10.86.5.59",
`ia-accept-language` CHAR(50) COMMENT "Currently not used",
`needcache` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-musicshop-version` CHAR(50) COMMENT "Currently not used",
`ia-x-nokia-musicshop-bearer` CHAR(50) COMMENT "Currently not used",
`ia-x-mdg-user` CHAR(50) COMMENT "Currently not used",
`ia-x-wapipaddr` CHAR(50) COMMENT "Currently not used",
`visitdsttime` CHAR(50) COMMENT "Currently not used",
`ia-x-amobee-1` CHAR(50) COMMENT "Currently not used",
`ia-wapconnection` CHAR(50) COMMENT "Currently not used",
`uip` CHAR(50) COMMENT "Currently not used",
`ia-x-vf-acr` CHAR(50) COMMENT "Currently not used",
`ia-x-via` CHAR(50) COMMENT "Currently not used",
`x-ia-pricing` CHAR(5) COMMENT "Response pricing model: CPC, CPM",
`x-ia-pricing-value` FLOAT COMMENT "Response price. Currently divided into 1,000 for CPM responses",
`x-ia-pricing-currency` CHAR(50) COMMENT "Always USD at the moment",
`x-ia-ad-width` INT(4) COMMENT "Returned ad width in px",
`x-ia-ad-height` INT(4) COMMENT "Returned ad height in px",
`x-ia-cid` CHAR(50) COMMENT "Currently not used",
`x-ia-session` CHAR(50) COMMENT "Currently not used",
`x-ia-error` CHAR(50) COMMENT "Error: OK | House Ad | Internal error | ... others",
`x-ia-adnetwork` CHAR(50) COMMENT "The demand partner who responded. For the errors/house ads it is always 'Inneractive 360'",
`x-ia-ad-type` INT(3) COMMENT "?? Returned ad type"
) ENGINE=BRIGHTHOUSE; 

set @bh_dataformat = 'txt_variable';
set @BH_REJECT_FILE_PATH = 'C:/usr/david/Inneractive/R\&D/Tech/Data/logs/reject_file.log';
set @BH_ABORT_ON_COUNT = 1000;
LOAD DATA INFILE 'C:/Temp/parsed.log' INTO TABLE AccessLog FIELDS TERMINATED BY ',' ENCLOSED BY '\''  LINES TERMINATED BY '\n';
*/